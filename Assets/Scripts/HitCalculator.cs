﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HitCalculator : MonoBehaviour
{
    public ModifierGroup[] modifierGroups;
    public Slider[] modifierSliders;

    public Text ToHitText;

	protected virtual void Start()
    {
        for(int i = 0; i < modifierGroups.Length; i++)
        {
            modifierGroups[i].ModiferGroupChanged += CalculateAggregate;
        }
    }

    public void CalculateAggregate()
    {
        int toHit = 0;

        for (int i = 0; i < modifierGroups.Length; i++)
        {
            toHit += modifierGroups[i].MyValue;
        }

        for(int j = 0; j < modifierSliders.Length; j++)
        {
            toHit += (int)modifierSliders[j].value;
        }

        toHit = Mathf.Clamp(toHit, 0, 20);

        string colorString = "<color=lime>";

        if(toHit <= 2)
        {
            colorString = "<color=yellow>";
        }
        if(toHit > 12)
        {
            colorString = "<color=red>";
        }

        ToHitText.text = string.Format("{0}TO HIT: {1}</color>",colorString, toHit);
    }
}
