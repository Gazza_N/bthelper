﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ModifierGroup : MonoBehaviour
{
    public event Action ModiferGroupChanged;

    private int myValue;

    public int MyValue
    {
        get { return myValue; }
    }

    public void SetValue(int value)
    {
        myValue = value;

        if(ModiferGroupChanged != null)
        {
            ModiferGroupChanged();
        }
    }
}
