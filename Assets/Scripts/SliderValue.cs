﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderValue : MonoBehaviour
{
    public Slider mySlider;

    private Text myText;
	
	void Start ()
    {
        myText = GetComponent<Text>();
        OnSliderChange();	
	}
	
    public void OnSliderChange()
    {
        myText.text = mySlider.value.ToString();
    }
}
