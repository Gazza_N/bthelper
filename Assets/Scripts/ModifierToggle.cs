﻿using UnityEngine;
using UnityEngine.UI;

public class ModifierToggle : MonoBehaviour
{
    public int value;

    public ModifierGroup myGroup;

    protected void Start()
    {
        if(GetComponent<Toggle>().isOn)
        {
            OnToggle();
        }
    }

    public void OnToggle()
    {
        myGroup.SetValue(value);
    }
}
