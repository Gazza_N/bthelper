﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class DamageCalculator : MonoBehaviour
{
    public ModifierGroup damageArc;

    public Text outputText;

    public GameObject attackButtons;

    public GameObject meleeButtons;

    private int arcIndex;

    private Dictionary<int,string>[] locationDictionary;

    private Dictionary<int, string>[] punchDictionary;

    private Dictionary<int, string>[] kickDictionary;

    private Dictionary<int, int[]> clusteringTables;

    public void Show(bool melee)
    {
        gameObject.SetActive(true);

        attackButtons.SetActive(!melee);
        meleeButtons.SetActive(melee);
    }

    public void OnPunchButton()
    {
        string location = "Deal punch damage to " + punchDictionary[arcIndex][RollDSix()];

        outputText.text = string.Format("<b>Punch Attack</b>\n{0}\n\n{1}", DateTime.Now.ToLongTimeString(), location);
    }

    public void OnKickButton()
    {
        string location = "Deal kick damage to " + kickDictionary[arcIndex][RollDSix()];

        outputText.text = string.Format("<b>Kick Attack</b>\n{0}\n\n{1}", DateTime.Now.ToLongTimeString(), location);
    }

    public void DeeEffAye()
    {
        outputText.text = string.Format("<b>DFA</b>\n{0}\n\n{1}", DateTime.Now.ToLongTimeString(), "Apply DFA damage, and deal kick damage to your legs.");
    }
    
    public void OnStandardButton()
    {
        outputText.text = string.Format("<b>Standard Attack</b>\n{0}\n\n{1}", DateTime.Now.ToLongTimeString(),RollLocation());
    }

    public void OnCritButton()
    {
        int crits = CritsToRoll();
        string critMessage;

        if (crits < 3)
        {
            critMessage = string.Format("<i>Roll {0} crit locations </i>", crits);
        }
        else
        {
            critMessage = "<i>3 crits to LT/CT/RT\nor\nDestroy Leg/Arm/Head</i>";
        }


        outputText.text = string.Format("<b>Internal damage</b>\n{0}\n\n{1}", DateTime.Now.ToLongTimeString(),critMessage);
    }

    public void OnLRMButton(int totalMissiles)
    {
        int hits = RollClustering(totalMissiles);

        string damageText = string.Format("<b>LRM {0}</b>\n{1}\n{2} hits\n\n", totalMissiles, DateTime.Now.ToLongTimeString(), hits);

        while(hits > 0)
        {
            if((hits - 5)>0)
            {
                damageText += string.Format("5 damage to {0}\n", RollLocation());

                hits -= 5;
            }
            else
            {
                damageText += string.Format("{0} damage to {1}\n", hits, RollLocation());

                hits =0;
            }
        }

        outputText.text = damageText;
    }

    public void OnSRMButton(int totalMissiles)
    {
        int hits = RollClustering(totalMissiles);

        string damageText = string.Format("<b>SRM {0}</b>\n{1}\n{2} hits\n\n",totalMissiles, DateTime.Now.ToLongTimeString(), hits);

        for (int i = 0; i < hits; i++)
        {
            damageText += string.Format("2 damage to {0}\n", RollLocation());
        }

        outputText.text = damageText;
    }

    private int Roll()
    {
        return (Random.Range(1, 7) + Random.Range(1,7));
    }

    private int RollDSix()
    {
        return Random.Range(1, 7);
    }

    private int RollClustering(int totalMissiles)
    {
        return clusteringTables[totalMissiles][Roll() - 2];
    }

    private string RollLocation()
    {
        int roll = Roll();

        string text = locationDictionary[arcIndex][roll];

        if (roll == 2)
        {
            text += string.Format("\n<i>Roll {0} critical hits</i>", CritsToRoll());
        }

        return text;
    }

    private void Awake()
    {
        damageArc.ModiferGroupChanged += ArcChanged;

        //Location tables
        locationDictionary = new Dictionary<int, string>[3];

        for (int i = 0; i < 3; i++)
        {
            locationDictionary[i] = new Dictionary<int, string>(11);

            switch (i)
            {
                case 0:

                    locationDictionary[i].Add(2, "Left Torso *Crit*");
                    locationDictionary[i].Add(3, "Left Leg");
                    locationDictionary[i].Add(4, "Left Arm");
                    locationDictionary[i].Add(5, "Left Arm");
                    locationDictionary[i].Add(6, "Left Leg");
                    locationDictionary[i].Add(7, "Left Torso");
                    locationDictionary[i].Add(8, "Centre Torso");
                    locationDictionary[i].Add(9, "Right Torso");
                    locationDictionary[i].Add(10, "Right Arm");
                    locationDictionary[i].Add(11, "Right Leg");
                    locationDictionary[i].Add(12, "Head");
                    break;

                case 1:
                    locationDictionary[i].Add(2, "Centre Torso *Crit*");
                    locationDictionary[i].Add(3, "Right Arm");
                    locationDictionary[i].Add(4, "Right Arm");
                    locationDictionary[i].Add(5, "Right Leg");
                    locationDictionary[i].Add(6, "Right Torso");
                    locationDictionary[i].Add(7, "Centre Torso");
                    locationDictionary[i].Add(8, "Left Torso");
                    locationDictionary[i].Add(9, "Left Leg");
                    locationDictionary[i].Add(10, "Left Arm");
                    locationDictionary[i].Add(11, "Left Arm");
                    locationDictionary[i].Add(12, "Head");
                    break;

                case 2:
                    locationDictionary[i].Add(2, "Right Torso *Crit*");
                    locationDictionary[i].Add(3, "Right Leg");
                    locationDictionary[i].Add(4, "Right Arm");
                    locationDictionary[i].Add(5, "Right Arm");
                    locationDictionary[i].Add(6, "Right Leg");
                    locationDictionary[i].Add(7, "Right Torso");
                    locationDictionary[i].Add(8, "Centre Torso");
                    locationDictionary[i].Add(9, "Left Torso");
                    locationDictionary[i].Add(10, "Left Arm");
                    locationDictionary[i].Add(11, "Left Leg");
                    locationDictionary[i].Add(12, "Head");
                    break;
            }
        }

        //Clustering tables

        clusteringTables = new Dictionary<int, int[]>(10);

        clusteringTables.Add(2, new int[] { 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2 });
        clusteringTables.Add(3, new int[] { 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3 });
        clusteringTables.Add(4, new int[] { 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4 });
        clusteringTables.Add(5, new int[] { 1, 2, 2, 3, 3, 3, 3, 4, 4, 5, 5 });
        clusteringTables.Add(6, new int[] { 2, 2, 3, 3, 4, 4, 4, 5, 5, 6, 6 });
        clusteringTables.Add(9, new int[] { 3, 3, 4, 5, 5, 5, 5, 7, 7, 9, 9 });
        clusteringTables.Add(10, new int[] { 3, 3, 4, 6, 6, 6, 6, 8, 8, 10, 10 });
        clusteringTables.Add(12, new int[] { 4, 4, 5, 8, 8, 8, 8, 10, 10, 12, 12 });
        clusteringTables.Add(15, new int[] { 5, 5, 6, 9, 9, 9, 9, 12, 12, 15, 15 });
        clusteringTables.Add(20, new int[] { 6, 6, 9, 12, 12, 12, 12, 16, 16, 20, 20 });

        //Punch tables
       punchDictionary = new Dictionary<int, string>[3];

        for (int i = 0; i < 3; i++)
        {
            punchDictionary[i] = new Dictionary<int, string>(11);

            switch (i)
            {
                case 0:

                    punchDictionary[i].Add(1, "Left Torso");
                    punchDictionary[i].Add(2, "Left Torso");
                    punchDictionary[i].Add(3, "Centre Torso");
                    punchDictionary[i].Add(4, "Left Arm");
                    punchDictionary[i].Add(5, "Left Arm");
                    punchDictionary[i].Add(6, "Head");
                    
                    break;

                case 1:
                    punchDictionary[i].Add(1, "Left Arm");
                    punchDictionary[i].Add(2, "Left Torso");
                    punchDictionary[i].Add(3, "Centre Torso");
                    punchDictionary[i].Add(4, "Right Torso");
                    punchDictionary[i].Add(5, "Right Arm");
                    punchDictionary[i].Add(6, "Head");

                    break;

                case 2:
                    punchDictionary[i].Add(1, "Right Torso");
                    punchDictionary[i].Add(2, "Right Torso");
                    punchDictionary[i].Add(3, "Centre Torso");
                    punchDictionary[i].Add(4, "Right Arm");
                    punchDictionary[i].Add(5, "Right Arm");
                    punchDictionary[i].Add(6, "Head");

                    break;
            }
        }

        //Kick tables
        kickDictionary = new Dictionary<int, string>[3];

        for (int i = 0; i < 3; i++)
        {
            kickDictionary[i] = new Dictionary<int, string>(11);

            switch (i)
            {
                case 0:

                    kickDictionary[i].Add(1, "Left Leg");
                    kickDictionary[i].Add(2, "Left Leg");
                    kickDictionary[i].Add(3, "Left Leg");
                    kickDictionary[i].Add(4, "Left Leg");
                    kickDictionary[i].Add(5, "Left Leg");
                    kickDictionary[i].Add(6, "Left Leg");

                    break;

                case 1:
                    kickDictionary[i].Add(1, "Right Leg");
                    kickDictionary[i].Add(2, "Right Leg");
                    kickDictionary[i].Add(3, "Right Leg");
                    kickDictionary[i].Add(4, "Left Leg");
                    kickDictionary[i].Add(5, "Left Leg");
                    kickDictionary[i].Add(6, "Left Leg");

                    break;

                case 2:
                    kickDictionary[i].Add(1, "Right Leg");
                    kickDictionary[i].Add(2, "Right Leg");
                    kickDictionary[i].Add(3, "Right Leg");
                    kickDictionary[i].Add(4, "Right Leg");
                    kickDictionary[i].Add(5, "Right Leg");
                    kickDictionary[i].Add(6, "Right Leg");

                    break;
            }
        }


            ArcChanged();
    }

    private void OnDestroy()
    {
        damageArc.ModiferGroupChanged -= ArcChanged;
    }

    private void ArcChanged()
    {
        arcIndex = damageArc.MyValue;
    }

    private int CritsToRoll()
    {
        int roll = Roll();

        if(roll <= 7)
        {
            return 0;
        }

        else if(roll <= 9)
        {
            return 1;
        }

        else if(roll <= 11)
        {
            return 2;
        }

        return 3;
    }
}
